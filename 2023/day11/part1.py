import numpy as np

input_path = r"2023/day11/input.txt"
# input_path = r"2023/day11/test_input.txt"

lines = list()

with open(input_path) as input_file:
    for line in input_file:
        lines.append([chr for chr in line.strip()])

# Create all of space.
space = np.array(lines)
# This is how big space is.
rows, cols = space.shape

# Space expands on empty rows and cols. So we have to find those.
clear_rows = list()
for row in range(rows):
    if np.all(space[row, :] == "."):
        clear_rows.append(row)

clear_cols = list()
for col in range(cols):
    if np.all(space[:, col] == "."):
        clear_cols.append(col)

# Now that we've found the empty space. We expand it.
empty_row = np.array(["."] * cols)

# If we do this in reverse we don't need to change the later indices when we add data.
for row in reversed(clear_rows):
    space = np.insert(space, row, empty_row, axis=0)

# We changed the shape by inserting rows. Get the new size (the cols should be the same).
rows, cols = space.shape
empty_col = np.array([["."]] * rows)
empty_col = np.reshape(empty_col, (1, -1))

# If we do this in reverse we don't need to change the later indices when we add data.
for col in reversed(clear_cols):
    space = np.insert(space, col, empty_col, axis=1)

# Find every galaxy in space.
galaxy_coods = np.where(space == "#")

total_dist = 0
for i, (row, col) in enumerate(zip(galaxy_coods[0][:-1], galaxy_coods[1][:-1])):
    # pair this galaxy with all the other ones counting up. That way we only do every pair once.
    for j in range(i + 1, len(galaxy_coods[0])):
        row1, col1 = galaxy_coods[0][j], galaxy_coods[1][j]
        dist = np.abs(row1 - row) + np.abs(col1 - col)
        total_dist += dist

print(total_dist)
