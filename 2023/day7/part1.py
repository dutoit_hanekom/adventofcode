from enum import Enum
from typing import Optional, List
from matplotlib import pyplot as plt
import math

input_path = "2023/day7/input.txt"
# input_path = "2023/day7/test_data.txt"

card_value_lookup = {
    "A": 14,
    "K": 13,
    "Q": 12,
    "J": 11,
    "T": 10,
    "9": 9,
    "8": 8,
    "7": 7,
    "6": 6,
    "5": 5,
    "4": 4,
    "3": 3,
    "2": 2,
}


class Rank(Enum):
    # Auto() ??
    high_card = 0
    pair = 1
    two_pair = 2
    three_of_a_kind = 3
    full_house = 4
    four_of_a_kind = 5
    five_of_a_kind = 6


class Hand:
    def __init__(self, cards: str, bid_: int):
        # Build the tie-break hand first. This is just in case we sort our hand later for display. The tie breaks are
        # done on the order the cards were in originally, not by any sorted order.
        self.tie_break_hand = [card_value_lookup[card] for card in cards]
        self.hand = cards
        self.rank = self.get_rank()
        self.bid: int = bid_

    def __str__(self):
        return f"{self.hand} : {str(self.rank)} - {self.bid}"

    def __eq__(self, other):
        # Two hands are never the same. (right?!)
        return False

    def __lt__(self, other):
        # No need for a tie-break, the ranks are different:
        if self.rank.value != other.rank.value:
            return self.rank.value < other.rank.value

        # Tie-break based on the order of all the cards.
        for self_card, other_card in zip(self.tie_break_hand, other.tie_break_hand):
            if self_card == other_card:
                continue
            else:
                return self_card < other_card

        raise ValueError("No two hands can be the same")

    def get_rank(self):
        sets = dict()
        for char in self.hand:
            if char not in sets.keys():
                sets[char] = 1
            else:
                sets[char] += 1

        values = sorted(list(sets.values()), reverse=True)

        # Look at the max value to see what the maximum size of our set is.
        match values[0]:
            case 5:
                rank = Rank.five_of_a_kind
            case 4:
                rank = Rank.four_of_a_kind
            case 3:
                # We may upgrade this later.
                rank = Rank.three_of_a_kind
            case 2:
                # We may upgrade this later.
                rank = Rank.pair
            case 1:
                rank = Rank.high_card
            case _:
                rank = Rank.high_card

        if rank == Rank.three_of_a_kind:
            # We also have a pair in this 3-of-a-kind hand.
            if values[1] == 2:
                # Upgrade rank to full house.
                rank = Rank.full_house

        if rank == Rank.pair:
            # If we have a pair, check if we have another.
            if values[1] == 2:
                # Then upgrade to two pair if we do.
                rank = Rank.two_pair

        return rank


hands: List[Hand] = list()

with open(input_path) as input_file:
    for line in input_file:
        tokens = line.strip().split(" ")
        hand_str: str = tokens[0]
        bid = int(tokens[1])

        hands.append(Hand(hand_str, bid))

hands = sorted(hands, reverse=False)

winnings = [(i + 1) * hand.bid for i, hand in enumerate(hands)]
# reverse_hands = sorted(hands, reverse=True)

print(f"total winnings: {sum(winnings)}")


# hmmmm.... That's not the right answer; your answer is too low.
# That's for: total winnings: 253690717
# Ah, I got the tie-breaks wrong. Cards must remain in order. You don't sort and then compare.
# hmmmm.... That's not the right answer; your answer is too high.
# That's for: total winnings: 254033608
# What case am I missing? I think there are zero 5-of-a-kind. That's fine, tho?
# AAAAh - I got the Enums wrong! I made 5-kind the same value is 4-kind. Dumb... Use Auto for enums!!!
# Correct answer: 254024898
