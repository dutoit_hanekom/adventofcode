# Day 7 Part 2.
import numpy as np

lookup = {"0": "+", "1": "*", "2": "|"}

def read_input(file_path):
    data_set = list()
    with open(file_path, "r") as file_handle:
        for line in file_handle:
            answer, vals_ = line.strip().split(":")
            vals = [int(val) for val in vals_.split(" ") if val != ""]
            data_set.append((int(answer), vals))

    return data_set

def eval_(nums: list[int], ops: list[str]) -> int:
    assert len(nums) - 1 == len(ops)

    ans = nums[0]

    for num, op in zip(nums[1:], ops):
        if op == "*":
            ans *= num
        elif op == "+":
            ans += num
        elif op == "|":
            ans = int(str(ans) + str(num))

    return  ans

def incr_ops(length, number):
    ternary = np.base_repr(number, base=3)
    padding = "0" * (length - len(ternary))
    ops = [lookup[chr_] for chr_ in (padding + ternary)]

    return ops

def part2(path):
    calib_data = read_input(path)

    score = 0

    for ans, nums in calib_data:
        # We'll need this many ops.
        num_ops = len(nums) - 1

        for i in range(3**num_ops):
            ops = incr_ops(num_ops, i)
            if eval_(nums, ops) == ans:
                score += ans
                # This one works - so skip the rest
                break

        print(f"Solved")
    print(f"{score=}")


if __name__ == "__main__":
    # The test result should be 11387.
    # part2("test_input.txt")

    # Correct: 149956401519484
    part2("input.txt")