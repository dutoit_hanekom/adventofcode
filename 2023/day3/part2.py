import numpy as np

input_path = r"/home/dutoit/PycharmProjects/aoc2023/day3/input.txt"
# input_path = r"/home/dutoit/PycharmProjects/aoc2023/day3/test_input.txt"

symbols = {"+", "*", "%", "@", "=", "&", "/", "#", "$", "-"}
gears = {"*"}

num_grid = np.zeros(shape=(140, 140), dtype=int)
symbol_points = list()
gear_points = list()

current_num = ""

with open(input_path) as input_file:
    for i, line in enumerate(input_file):
        for j, char in enumerate(line):
            # If we find a new number:
            if char.isnumeric() and current_num == "":
                current_num += char
                start_idx = j
            # If we are continuing with a number:
            elif char.isnumeric() and current_num != "":
                current_num += char
            # We're at the end of a number:
            elif current_num != "" and not char.isdigit():
                number = int(current_num)
                end_idx = j
                num_grid[i, start_idx:end_idx] = number
                current_num = ""

            # record where the symbols are:
            if char in symbols:
                symbol_points.append((i, j))
            if char in gears:
                gear_points.append((i, j))


total = 0
for point in gear_points:
    surrounding_nums = set()
    i_, j_ = point
    for i in range(i_ - 1, i_ + 2):
        for j in range(j_ - 1, j_ + 2):
            num = num_grid[i, j]
            surrounding_nums.add(num)

    surrounding_nums.remove(0)
    if len(surrounding_nums) == 2:
        gear_ratio = surrounding_nums.pop() * surrounding_nums.pop()
        total += gear_ratio


# print(num_grid)
# print(symbol_points)
print(total)
