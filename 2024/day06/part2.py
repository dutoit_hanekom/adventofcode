# Day 6, part 2.
import numpy as np
from enum import Enum
from timeit import default_timer

from joblib import Parallel, delayed


class Dir(Enum):
    up = 0
    down = 1
    left = 2
    right = 3


look = {
    Dir.up: np.array([-1, 0]),
    Dir.down: np.array([1, 0]),
    Dir.left: np.array([0, -1]),
    Dir.right: np.array([0, 1]),
}

# Order of rotations - always turn `right` aka `clockwise`
rotate = [Dir.up, Dir.right, Dir.down, Dir.left]


def read_input(file_path):
    data_set = list()
    with open(file_path, "r") as file_handle:
        for line in file_handle:
            data_set.append([char_ for char_ in line.strip()])

    return np.array(data_set)

def is_in_bounds(map_: np.ndarray, pos: np.ndarray) -> bool:
    assert pos.shape == (2,)

    width, height = map_.shape

    return bool(0 <= pos[0] < width and 0 <= pos[1] < height)


def find_path(map_, init_pos):
    pos = init_pos
    dir_ = Dir.up

    while is_in_bounds(map_, pos):
        map_[pos[0], pos[1]] = "X"
        next_pos = pos + look[dir_]

        # Is this dir in bounds?
        if not is_in_bounds(map_, next_pos):
            break

        next_block = map_[next_pos[0], next_pos[1]]

        if next_block in ["^", ".", "X"]:
            # Empty block - advance in this direction.
            pos = next_pos
        elif next_block == "#":
            dir_ = rotate[(rotate.index(dir_) + 1) % len(rotate)]

    return np.argwhere(map_ == "X")


def is_loop(map_, starting_pos) -> bool:
    pos = starting_pos
    dir_: Dir = Dir.up
    # state_map = np.zeros(shape=map_.shape, dtype=object)
    state_map = dict()

    while is_in_bounds(map_, pos):
        coord_str = f"{pos[0]}, {pos[1]}"
        current_state = state_map.get(coord_str, None)

        if current_state is None:
            state_map[coord_str] = [dir_]
        elif isinstance(current_state, list):
            if dir_ in current_state:
                # We've been here before in this state - it's a loop, so break.
                return True
            # Append the current state to check for later.
            current_state.append(dir_)

        next_pos = pos + look[dir_]

        # Is this dir in bounds?
        if not is_in_bounds(map_, next_pos):
            return False

        next_block = map_[next_pos[0], next_pos[1]]

        if next_block in ["^", ".", "X"]:
            # Empty block - advance in this direction.
            map_[next_pos[0], next_pos[1]] = "X"
            pos = next_pos
        elif next_block in ["#", "O"]:
            dir_ = rotate[(rotate.index(dir_) + 1) % len(rotate)]

    return False

def wrap(row, col, map_, starting_pos):
    new_map = map_.copy()
    new_map[row, col] = "O"

    loop = is_loop(new_map, starting_pos)
    # print(f"Done with {row=}, {col=}")
    return loop

def part2(file_path):
    map_ = read_input(file_path)

    # Find the starting pos and direction.
    starting_pos: np.ndarray = np.argwhere(map_ == "^").reshape(-1,)

    start_time = default_timer()
    possible_paths = find_path(map_.copy(), starting_pos)

    loops = Parallel(n_jobs=16)(delayed(wrap)(row, col, map_, starting_pos) for row, col in possible_paths)
    end_time = default_timer()

    calc_time = end_time - start_time


    # print(map_)
    print(f"{sum(loops)}")
    print(f"Time: {calc_time}")
    return loops


if __name__ == "__main__":
    # The test result should be 6.
    # part2("test_input.txt")

    # Correct: 1705
    part2("input.txt")
