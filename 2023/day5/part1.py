from enum import Enum
from typing import Optional, List, Dict

input_path = "2023/day5/input.txt"
# input_path = "2023/day5/test_data.txt"


class Mode(Enum):
    # Auto() ??
    seeds = 0
    seed_to_soil = 1
    soil_to_fertilizer = 2
    fertilizer_to_water = 3
    water_to_light = 4
    light_to_temperature = 5
    temperature_to_humidity = 6
    humidity_to_location = 7


class WeirdMap:
    def __init__(
        self,
        source_starts: Optional[List[int]] = None,
        dest_starts: Optional[List[int]] = None,
        spans: Optional[List[int]] = None,
    ):
        if source_starts is not None:
            assert len(source_starts) == len(dest_starts) == len(spans)

        self.source_starts: Optional[List[int]] = source_starts
        self.dest_starts: Optional[List[int]] = dest_starts
        self.spans: Optional[List[int]] = spans

        self.sorted = False

    def add_map(self, source_start: int, dest_start: int, span: int):
        if self.source_starts is None or self.dest_starts is None or self.spans is None:
            self.source_starts = list()
            self.dest_starts = list()
            self.spans = list()

        self.source_starts.append(source_start)
        self.dest_starts.append(dest_start)
        self.spans.append(span)

        # Once we add a map we can't be sure they're sorted anymore.
        self.sorted = False

    def map_(self, num):
        for source_start, dest_start, span in zip(self.source_starts, self.dest_starts, self.spans):
            if source_start <= num < source_start + span:
                gap = num - source_start
                return dest_start + gap
        return num

    def sort_map(self):
        combined_lists = list(zip(self.source_starts, self.dest_starts, self.spans))
        sorted_combined_lists = sorted(combined_lists, key=lambda x: x[0])
        self.source_starts, self.dest_starts, self.spans = zip(*sorted_combined_lists)
        self.source_starts, self.dest_starts, self.spans = (
            list(self.source_starts),
            list(self.dest_starts),
            list(self.spans),
        )
        self.sorted = True


seed_to_soil_map: Optional[WeirdMap] = WeirdMap(None, None, None)
soil_to_fertilizer_map: Optional[WeirdMap] = WeirdMap(None, None, None)
fertilizer_to_water_map: Optional[WeirdMap] = WeirdMap(None, None, None)
water_to_light_map: Optional[WeirdMap] = WeirdMap(None, None, None)
light_to_temperature_map: Optional[WeirdMap] = WeirdMap(None, None, None)
temperature_to_humidity_map: Optional[WeirdMap] = WeirdMap(None, None, None)
humidity_to_location_map: Optional[WeirdMap] = WeirdMap(None, None, None)

seeds = None

mode = Mode.seeds
with open(input_path) as input_file:
    for k, line in enumerate(input_file):
        # Skip blank lines.
        if line == "\n":
            continue

        # We start in this mode, but let's handle it anyway
        if "seeds:" in line:
            mode = Mode.seeds
            seeds = line.split(" ")
            seeds = [int(token.strip()) for token in seeds if token.strip().isnumeric()]
            continue
        elif "seed-to-soil map" in line:
            mode = Mode.seed_to_soil
            continue
        elif "soil-to-fertilizer map" in line:
            mode = Mode.soil_to_fertilizer
            continue
        elif "fertilizer-to-water map" in line:
            mode = Mode.fertilizer_to_water
            continue
        elif "water-to-light map" in line:
            mode = Mode.water_to_light
            continue
        elif "light-to-temperature map" in line:
            mode = Mode.light_to_temperature
            continue
        elif "temperature-to-humidity map" in line:
            mode = Mode.temperature_to_humidity
            continue
        elif "humidity-to-location map" in line:
            mode = Mode.humidity_to_location
            continue

        dest_start, source_start, span = line.split(" ")
        dest_start, source_start, span = int(dest_start.strip()), int(source_start.strip()), int(span.strip())

        if mode == Mode.seed_to_soil:
            seed_to_soil_map.add_map(source_start, dest_start, span)
        elif mode == Mode.soil_to_fertilizer:
            soil_to_fertilizer_map.add_map(source_start, dest_start, span)
        elif mode == Mode.fertilizer_to_water:
            fertilizer_to_water_map.add_map(source_start, dest_start, span)
        elif mode == Mode.water_to_light:
            water_to_light_map.add_map(source_start, dest_start, span)
        elif mode == Mode.light_to_temperature:
            light_to_temperature_map.add_map(source_start, dest_start, span)
        elif mode == Mode.temperature_to_humidity:
            temperature_to_humidity_map.add_map(source_start, dest_start, span)
        elif mode == Mode.humidity_to_location:
            humidity_to_location_map.add_map(source_start, dest_start, span)


locs = list()
for seed in seeds:
    soil_num = seed_to_soil_map.map_(seed)
    fert_num = soil_to_fertilizer_map.map_(soil_num)
    water_num = fertilizer_to_water_map.map_(fert_num)
    light_num = water_to_light_map.map_(water_num)
    temp_num = light_to_temperature_map.map_(light_num)
    hum_num = temperature_to_humidity_map.map_(temp_num)
    loc_num = humidity_to_location_map.map_(hum_num)

    locs.append(loc_num)

print(min(locs))
