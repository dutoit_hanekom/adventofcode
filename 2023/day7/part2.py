from enum import Enum
from typing import Optional, List
from matplotlib import pyplot as plt
import math
from collections import Counter

input_path = "2023/day7/input.txt"
# input_path = "2023/day7/test_data.txt"

card_value_lookup = {
    "A": 14,
    "K": 13,
    "Q": 12,
    "J": 1,  # These are now jokers.
    "T": 10,
    "9": 9,
    "8": 8,
    "7": 7,
    "6": 6,
    "5": 5,
    "4": 4,
    "3": 3,
    "2": 2,
}


class Rank(Enum):
    # Auto() ??
    high_card = 0
    pair = 1
    two_pair = 2
    three_of_a_kind = 3
    full_house = 4
    four_of_a_kind = 5
    five_of_a_kind = 6


class Hand:
    def __init__(self, cards: str, bid_: int):
        # Build the tie-break hand first. This is just in case we sort our hand later for display. The tie breaks are
        # done on the order the cards were in originally, not by any sorted order.
        self.tie_break_hand = [card_value_lookup[card] for card in cards]
        self.hand = cards
        self.rank = self.get_rank()
        self.bid: int = bid_

    def __str__(self):
        return f"{self.hand}: {str(self.rank)} - {self.bid}"

    def __eq__(self, other):
        # Two hands are never the same. (right?!)
        return False

    def __lt__(self, other):
        # No need for a tie-break, the ranks are different:
        if self.rank.value != other.rank.value:
            return self.rank.value < other.rank.value

        # Tie-break based on the order of all the cards.
        for self_card, other_card in zip(self.tie_break_hand, other.tie_break_hand):
            if self_card == other_card:
                continue
            else:
                return self_card < other_card

        raise ValueError("No two hands should ever be the same")

    def get_rank(self):
        sets = Counter(self.hand)

        # Special case for 5 jokers. The method adds the number of jokers to the next highest card, so if there are
        # only jokers in the hand, this fails.
        if sets.get("J", 0) == 5:
            return Rank.five_of_a_kind

        # Keep count of jokers separately. 'Cause we can always add jokers to something else to make it better.
        jokers = sets.pop("J", 0)

        counts = sorted(list(sets.values()), reverse=True)
        # Add the jokers to the highest bin cards we have for the best upgrade.
        counts[0] += jokers

        # Look at the max value to see what the maximum size of our set is.
        match counts:
            case [5, *_]:
                rank = Rank.five_of_a_kind
            case [4, *_]:
                rank = Rank.four_of_a_kind
            case [3, 2, *_]:
                rank = Rank.full_house
            case [3, *_]:
                rank = Rank.three_of_a_kind
            case [2, 2, *_]:
                rank = Rank.two_pair
            case [2, *_]:
                rank = Rank.pair
            case [1, *_]:
                rank = Rank.high_card
            case _:
                # This shouldn't happen?
                raise ValueError("We can't get a hand like this (Except for JJJJJ - but handle that sooner).")

        return rank


hands: List[Hand] = list()

with open(input_path) as input_file:
    for line in input_file:
        tokens = line.strip().split(" ")
        hand_str: str = tokens[0]
        bid = int(tokens[1])

        hands.append(Hand(hand_str, bid))

hands = sorted(hands, reverse=False)

winnings = [(i + 1) * hand.bid for i, hand in enumerate(hands)]

print(f"total winnings: {sum(winnings)}")

# Make sure we get the correct answers after trying to optimise.
assert sum(winnings) == 254115617

# Correct answer: 254115617
