import numpy as np

input_path = r"/home/dutoit/PycharmProjects/aoc2023/day9/input.txt"
# input_path = r"/home/dutoit/PycharmProjects/aoc2023/day9/test_input.txt"

seqs = list()

with open(input_path) as input_file:
    for line in input_file:
        seq = line.split(" ")
        seq = [int(num.strip()) for num in seq]

        seqs.append(seq)

total_puzzle_value = 0
for sequence in seqs:
    work_set = sequence

    all_diffs_zero = False
    diffs = list()

    while not all_diffs_zero:
        diff = [a - b for a, b in zip(work_set[1:], work_set[:-1])]
        diffs.append(diff)
        all_diffs_zero = all([val == 0 for val in diff])
        work_set = diff

    diffs.reverse()
    diffs.append(sequence)
    for diff0, diff1 in zip(diffs[:-1], diffs[1:]):
        next_val = diff1[-1] + diff0[-1]
        diff1.append(next_val)
        print("breakpoint")

    # puzzle_val = diffs[-1][-1]
    print(f"Next val {next_val}")
    total_puzzle_value += next_val

print(total_puzzle_value)
