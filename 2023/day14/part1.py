import numpy as np

# input_path = r"2023/day14/test_input.txt"
input_path = r"2023/day14/input.txt"

grid_list = list()
with open(input_path) as input_file:
    for line in input_file:
        grid_list.append([ch_ for ch_ in line.strip()])

grid = np.array(grid_list)

rows, cols = grid.shape

for row in range(1, rows):  # skip the top row.
    for col in range(0, cols):
        # If it's a round rock...
        if grid[row, col] == "O":
            # ... then we have to search up to see where the top-most empty spot is.
            i = 1
            for i in range(1, row + 1):
                if grid[row - i, col] != ".":
                    break  # we found an obstacle.
            else:
                i += 1
            grid[row, col] = "."
            grid[row - i + 1, col] = "O"

load = 0
for i, row in enumerate(grid):
    load += (rows - i) * np.sum(np.equal(row, "O"))

print(load)