from pathlib import Path
from os.path import dirname, realpath

from datetime import datetime

today = datetime.today()
year, day = today.year, today.day

# This file is in the root.
project_root_path = Path(dirname(realpath(__file__)))

# Today's folder path is:
folder_path = project_root_path / f"{year}" / f"day{day:02d}"

folder_path.mkdir(parents=True, exist_ok=True)

code_stub = """# Day {day} Part {part}.

def read_input(file_path):
    data_set = list()
    with open(file_path, "r") as file_handle:
        for line in file_handle:
            data_set = [int(chr_) for chr_ in line.strip()]

    return data_set

def part{part}(data):
    
    score = 0
    # Process score here.

    print(f"score: ")

if __name__ == "__main__":
    # The test result should be _.
    data_ = read_input("test_input.txt")

    # Correct: _.
    # data_ = read_input("input.txt")

    part{part}(data_)
"""

for part in [1, 2]:
    py_file = folder_path / f"part{part}.py"
    if py_file.exists():
        # It exists, continue so we don't overwrite it.
        continue

    with open(py_file, mode="w") as file_handle:
        file_handle.write(code_stub.format(day=day, part=part))


for file_name in ["input.txt",  "test_input.txt"]:
    data_file = folder_path / file_name
    if not data_file.exists():
        with open(data_file, mode="w") as file_handle:
            file_handle.write(f"Put your data here.")

print(f"You are now ready to work in: {folder_path}")
