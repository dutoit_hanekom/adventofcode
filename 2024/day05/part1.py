# Day 5, part 1.


def read_input(file_path):
	order = list()
	updates = list()
	with open(file_path, "r") as file_handle:
		for line in file_handle:
			if "|" in line:
				order.append(tuple([int(val) for val in line.strip().split("|")]))
			elif "," in line:
				updates.append([int(val) for val in line.strip().split(",")])


	return order, updates

def verify_update_rule(update_list: list, rules: list[tuple[int, int]]) -> bool:

	for i, val in enumerate(update_list):
		next_list = update_list[i+1:]

		for rule in rules:
			if rule[0] == val:
				if rule[1] not in update_list:
					continue
				if rule[1] not in next_list:
					return False

	return True


def part1(file_path):
	order, updates = read_input(file_path)
	total_score = 0

	for update in updates:
		result = verify_update_rule(update, order)

		if result:
			total_score += update[len(update) // 2]

		# print(f"breakpoint {result=}")

	# Process data here.

	print(f"{total_score=}")
	return total_score

if __name__ == "__main__":
	# The test result should be 143.
	# part1("test_input.txt")

	# Correct: 4637 (I think).
	part1("input.txt")