from enum import Enum
from typing import Optional, List
from matplotlib import pyplot as plt
import math

# input_path = "2023/day6/input.txt"
input_path = "2023/day6/test_data.txt"

with open(input_path) as input_file:
    times = input_file.readline()
    times = [int(token.strip()) for token in times.split(" ") if token.strip().isnumeric()]
    dists = input_file.readline()
    dists = [int(token.strip()) for token in dists.split(" ") if token.strip().isnumeric()]

winss = list()
for time, dist in zip(times, dists):
    i1 = (-time + math.sqrt(time * time - 4 * dist)) / -2
    i2 = (-time - math.sqrt(time * time - 4 * dist)) / -2
    print(i1, i2)
    print(i2 - i1)

    wins = 0
    for i in range(time):
        dist_traveled = (time - i) * i
        if dist_traveled > dist:
            wins += 1

    winss.append(wins)

total = winss[0]
for win in winss[1:]:
    total *= win
print(total)
