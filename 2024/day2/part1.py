# Day 2 part 1.

def read_input(file_path):
    reports = list()
    with open(file_path, "r") as file_handle:
        for line in file_handle:
            report = line.split(" ")
            report = [int(char.strip()) for char in report]
            reports.append(report)

    return reports

def part1(file_path):
    reports = read_input(file_path)

    safe_score = 0
    for report in reports:
        # Diffs in the values.
        creasing = [a - b for a, b in zip(report[1:], report[:-1])]

        # Monotonically increasing or decreasing?
        safe1 = all([val > 0 for val in creasing]) or all([val < 0 for val in creasing])

        # Max diff must be smaller equal to 3.
        safe2 = max([abs(val) for val in creasing]) <= 3

        if safe1 and safe2:
            safe_score += 1

    print(f"safe_score: {safe_score}")

if __name__ == "__main__":
    # The test result should be 2.
    # part1("test_input.txt")

    part1("input.txt")