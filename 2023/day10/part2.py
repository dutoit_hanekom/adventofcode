import numpy as np
from matplotlib import pyplot as plt
import cv2


input_path = r"2023/day10/input.txt"
# input_path = r"2023/day10/test_input.txt"

lines = list()

with open(input_path) as input_file:
    for line in input_file:
        lines.append([chr for chr in line.strip()])


def get_possible_next_pipes(nav_map, block_idx):
    """Looks at the block you're pointing at in the map and finds the two tiles this can go to."""
    block_type = nav_map[block_idx]
    row, col = block_idx
    if block_type == "S":
        # We need to search the four adjacent blocks to see what could connect to this.
        possibilities = list()
        # hmmm.... lets do this ugly first, optimise later.
        try:
            # Look left and check:
            if nav_map_[row, col - 1] == "-" or nav_map_[row, col - 1] == "L" or nav_map_[row, col - 1] == "F":
                possibilities.append((row, col - 1))
        except IndexError:
            pass

        try:
            # Look right and check:
            if nav_map_[row, col + 1] == "-" or nav_map_[row, col + 1] == "J" or nav_map_[row, col + 1] == "7":
                possibilities.append((row, col + 1))
        except IndexError:
            pass

        try:
            # Look up and check:
            if nav_map_[row - 1, col] == "|" or nav_map_[row - 1, col] == "F" or nav_map_[row - 1, col] == "7":
                possibilities.append((row - 1, col))
        except IndexError:
            pass

        try:
            # Look up and check:
            if nav_map_[row + 1, col] == "|" or nav_map_[row + 1, col] == "J" or nav_map_[row + 1, col] == "L":
                possibilities.append((row + 1, col))
        except IndexError:
            pass

        return possibilities
    elif block_type == "|":
        # You can go top and bottom from this one:
        return [(row - 1, col), (row + 1, col)]
    elif block_type == "-":
        # You can go left and right from this one:
        return [(row, col - 1), (row, col + 1)]
    elif block_type == "7":
        return [(row + 1, col), (row, col - 1)]
    elif block_type == "F":
        return [(row + 1, col), (row, col + 1)]
    elif block_type == "J":
        return [(row - 1, col), (row, col - 1)]
    elif block_type == "L":
        return [(row - 1, col), (row, col + 1)]

    return 0


nav_map_ = np.array(lines)

start_idx = np.where(nav_map_ == "S")

# We get the possibilities and just pick one direction to go in. Since this loops, all should be fine.
prev_block = start_idx
current_block = get_possible_next_pipes(nav_map_, start_idx)[0]

# Expand this as we go.
part_of_loop = [start_idx, current_block]

# Counter starts at one - we already made one step above.
counter = 1
# Now we loop through the pipe system until we get back to the start.
while nav_map_[current_block] != "S":
    # Get the two ways we can go in.
    ways = get_possible_next_pipes(nav_map_, current_block)

    # One of the ways is the way we came from (prev_idx). We must go in the other direction.
    way_to_go = [way for way in ways if way != prev_block][0]

    # Step to the next block.
    part_of_loop.append(way_to_go)
    prev_block = current_block
    current_block = way_to_go

    counter += 1

image = np.zeros_like(nav_map_, dtype=np.uint8)

# Now we've built the original big loop. I'm going to build a blownup version to add the boundaries around cells.
# Every odd row or column in this image represents the boundary around a pixel (or pipe section). This way we can follow
# the boundary to determine if a section is internal or external to the loop.
bigg = np.zeros_like(image, shape=(image.shape[0] * 2 + 1, image.shape[1] * 2 + 1))

for i, pix in enumerate(part_of_loop[:-1]):
    image[pix] = 255
    bigg[pix[0] * 2 + 1, pix[1] * 2 + 1] = 255

    next_ = part_of_loop[i + 1]
    # If you take the average of the two points and then project it to the BiGG image, this is what you get.
    mid_point = pix[0] + next_[0] + 1, pix[1] + next_[1] + 1
    bigg[mid_point] = 255

# Now, floodfill the image starting at the edge. Anything that connect to the edge can't be internal to the loop.
mask = np.zeros(shape=(bigg.shape[0] + 2, bigg.shape[1] + 2), dtype=np.uint8)
retval, bigg2, mask_out, rect = cv2.floodFill(bigg, mask, (0, 0), 127)

# cv2.imshow("BiGG", bigg)
# cv2.imshow("BiGG2", bigg2)
# cv2.imwrite("BiGG.bmp", bigg)
# cv2.imwrite("BiGG2.bmp", bigg2)
# cv2.imwrite("testing.bmp", image)
# cv2.imshow("testing", image)
# cv2.waitKey(-1)

# Check where every pixel in the source image maps to. If it's white (255) then it's a pipe and it doesn't have to be
# checked. If it maps to 127, it external to the loop. If it maps to 0 it's internal.
internal_points = 0
for row in range(image.shape[0]):
    for col in range(image.shape[1]):
        if image[row, col] == 255:
            continue
        maps_to = 2 * row + 1, 2 * col + 1
        if bigg[maps_to] == 0:
            internal_points += 1

# 444 is too low. That was cv2.contour area - counter/2
# 7589 is too high. That was cv2.contour area.
# HA! The correct answer was 445. So close to the opencv contour area calc. Out by 1....

print(internal_points)
