from enum import Enum
from typing import Optional, List
from matplotlib import pyplot as plt

input_path = "2023/day5/input.txt"
# input_path = "2023/day5/test_data.txt"


class WeirdMap:
    def __init__(
        self,
        source_starts: Optional[List[int]] = None,
        dest_starts: Optional[List[int]] = None,
        spans: Optional[List[int]] = None,
    ):
        if source_starts is not None:
            assert len(source_starts) == len(dest_starts) == len(spans)

        self.source_starts: Optional[List[int]] = source_starts
        self.dest_starts: Optional[List[int]] = dest_starts
        self.spans: Optional[List[int]] = spans

        self.sorted = False

    def add_map(self, source_start: int, dest_start: int, span: int):
        if self.source_starts is None or self.dest_starts is None or self.spans is None:
            self.source_starts = list()
            self.dest_starts = list()
            self.spans = list()

        self.source_starts.append(source_start)
        self.dest_starts.append(dest_start)
        self.spans.append(span)

        # Once we add a map we can't be sure they're sorted anymore.
        self.sorted = False

    def map_(self, num):
        for source_start, dest_start, span in zip(self.source_starts, self.dest_starts, self.spans):
            if source_start <= num < source_start + span:
                gap = num - source_start
                return dest_start + gap
        return num

    def sort_map(self):
        combined_lists = list(zip(self.source_starts, self.dest_starts, self.spans))
        sorted_combined_lists = sorted(combined_lists, key=lambda x: x[0])
        self.source_starts, self.dest_starts, self.spans = zip(*sorted_combined_lists)
        self.source_starts, self.dest_starts, self.spans = (
            list(self.source_starts),
            list(self.dest_starts),
            list(self.spans),
        )
        self.sorted = True

    def find_output_map_ranges(self, range_start, range_end):
        if not self.sorted:
            self.sort_map()

        bounding_points = [range_start]

        for source_start, dest_start, span in zip(self.source_starts, self.dest_starts, self.spans):
            source_end = source_start + span
            if range_start < source_start < range_end:
                bounding_points.append(source_start)
            if range_start < source_end < range_end:
                bounding_points.append(source_end)

        bounding_points.append(range_end)

        pairs = list(zip(bounding_points, bounding_points[1:]))

        output_ranges = [(self.map_(pair[0]), self.map_(pair[1])) for pair in pairs]

        return output_ranges


seed_to_soil_map: WeirdMap = WeirdMap(None, None, None)
soil_to_fertilizer_map: WeirdMap = WeirdMap(None, None, None)
fertilizer_to_water_map: WeirdMap = WeirdMap(None, None, None)
water_to_light_map: WeirdMap = WeirdMap(None, None, None)
light_to_temperature_map: WeirdMap = WeirdMap(None, None, None)
temperature_to_humidity_map: WeirdMap = WeirdMap(None, None, None)
humidity_to_location_map: WeirdMap = WeirdMap(None, None, None)

seed_starts: Optional[List[int]] = None
seed_ends: Optional[List[int]] = None


class Mode(Enum):
    # Auto() ??
    seeds = 0
    seed_to_soil = 1
    soil_to_fertilizer = 2
    fertilizer_to_water = 3
    water_to_light = 4
    light_to_temperature = 5
    temperature_to_humidity = 6
    humidity_to_location = 7


mode = Mode.seeds
with open(input_path) as input_file:
    for k, line in enumerate(input_file):
        # Skip blank lines.
        if line == "\n":
            continue

        # We start in this mode, but let's handle it anyway
        if "seeds:" in line:
            mode = Mode.seeds
            seeds = line.split(" ")
            seeds = [int(token.strip()) for token in seeds if token.strip().isnumeric()]

            seed_starts = [seed for i, seed in enumerate(seeds) if i % 2 == 0]
            seed_ranges = [seed for i, seed in enumerate(seeds) if i % 2 == 1]

            seed_ends = [seed + range_ for seed, range_ in zip(seed_starts, seed_ranges)]

            continue
        elif "seed-to-soil map" in line:
            mode = Mode.seed_to_soil
            continue
        elif "soil-to-fertilizer map" in line:
            mode = Mode.soil_to_fertilizer
            continue
        elif "fertilizer-to-water map" in line:
            mode = Mode.fertilizer_to_water
            continue
        elif "water-to-light map" in line:
            mode = Mode.water_to_light
            continue
        elif "light-to-temperature map" in line:
            mode = Mode.light_to_temperature
            continue
        elif "temperature-to-humidity map" in line:
            mode = Mode.temperature_to_humidity
            continue
        elif "humidity-to-location map" in line:
            mode = Mode.humidity_to_location
            continue

        dest_start, source_start, span = line.split(" ")
        dest_start, source_start, span = int(dest_start.strip()), int(source_start.strip()), int(span.strip())

        if mode == Mode.seed_to_soil:
            seed_to_soil_map.add_map(source_start, dest_start, span)
        elif mode == Mode.soil_to_fertilizer:
            soil_to_fertilizer_map.add_map(source_start, dest_start, span)
        elif mode == Mode.fertilizer_to_water:
            fertilizer_to_water_map.add_map(source_start, dest_start, span)
        elif mode == Mode.water_to_light:
            water_to_light_map.add_map(source_start, dest_start, span)
        elif mode == Mode.light_to_temperature:
            light_to_temperature_map.add_map(source_start, dest_start, span)
        elif mode == Mode.temperature_to_humidity:
            temperature_to_humidity_map.add_map(source_start, dest_start, span)
        elif mode == Mode.humidity_to_location:
            humidity_to_location_map.add_map(source_start, dest_start, span)

# for test_num in [0, 1, 48, 49, 50, 51, 52, 53, 96, 97, 98, 99, 100, 101, 125]:
#     output_num = seed_to_soil_map.map_(test_num)
#     print(f"{test_num} maps to {output_num}")

# plt.grid()
# plt.show()

# print("breakpoint")
soil_ranges = list()
fert_ranges = list()
water_ranges = list()
light_ranges = list()
temp_ranges = list()
hum_ranges = list()
loc_ranges = list()

# for source, dest, len_ in zip(seed_to_soil_map.source_starts, seed_to_soil_map.dest_starts, seed_to_soil_map.spans):
#     x1 = source
#     y1 = dest
#     x2 = source + len_
#     y2 = dest + len_
#     # print(f"{x1}, {y1}, {x2}, {y2}")
#     plt.plot((x1, x2), (y1, y2))

# seed_starts, seed_ends = [25], [125]  # TODO: Remove this hack
for seed_start, seed_end in zip(seed_starts, seed_ends):
    plt.plot((seed_start, seed_end), (0, 0), "b-o")
    ranges = seed_to_soil_map.find_output_map_ranges(seed_start, seed_end)
    soil_ranges += ranges
#
#     for range_ in soil_ranges:
#         x1 = 0
#         y1 = range_[0]
#         x2 = 0
#         y2 = range_[1]
#         # print(f"{x1}, {y1}, {x2}, {y2}")
#         plt.plot((x1, x2), (y1, y2), "r-o")
#
# plt.plot((0, 125), (0, 125), "c-")
# plt.grid()
# plt.show()

for soil in soil_ranges:
    ranges = soil_to_fertilizer_map.find_output_map_ranges(soil[0], soil[1])
    fert_ranges += ranges

for fert in fert_ranges:
    ranges = fertilizer_to_water_map.find_output_map_ranges(fert[0], fert[1])
    water_ranges += ranges

for water in water_ranges:
    ranges = water_to_light_map.find_output_map_ranges(water[0], water[1])
    light_ranges += ranges

for light in light_ranges:
    ranges = light_to_temperature_map.find_output_map_ranges(light[0], light[1])
    temp_ranges += ranges

for temp in temp_ranges:
    ranges = temperature_to_humidity_map.find_output_map_ranges(temp[0], temp[1])
    hum_ranges += ranges

for hum in hum_ranges:
    ranges = humidity_to_location_map.find_output_map_ranges(hum[0], hum[1])
    loc_ranges += ranges

mins = [loc[0] for loc in loc_ranges]
maxs = [loc[1] for loc in loc_ranges]

print(min(mins))
plt.plot(mins)
plt.show()

# hhmmm... this doesn't work.
