# Day 6, part 1.
import numpy as np
from enum import Enum


class Dir(Enum):
    up = 0
    down = 1
    left = 2
    right = 3


look = {
    Dir.up: np.array([-1, 0]),
    Dir.down: np.array([1, 0]),
    Dir.left: np.array([0, -1]),
    Dir.right: np.array([0, 1]),
}

# Order of rotations - always turn `right` aka `clockwise`
rotate = [Dir.up, Dir.right, Dir.down, Dir.left]


def read_input(file_path):
    data_set = list()
    with open(file_path, "r") as file_handle:
        for line in file_handle:
            data_set.append([char_ for char_ in line.strip()])

    return np.array(data_set)

def is_in_bounds(map_: np.ndarray, pos: np.ndarray) -> bool:
    assert pos.shape == (2,)

    width, height = map_.shape

    return bool(0 <= pos[0] < width and 0 <= pos[1] < height)


def part1(file_path):
    map_ = read_input(file_path)

    # Find the starting pos and direction.
    pos: np.ndarray = np.argwhere(map_ == "^").reshape(-1,)
    dir_: Dir = Dir.up

    while is_in_bounds(map_, pos):
        next_pos = pos + look[dir_]

        # Is this dir in bounds?
        if not is_in_bounds(map_, next_pos):
            break

        next_block = map_[next_pos[0], next_pos[1]]

        if next_block in ["^", ".", "X"]:
            # Empty block - advance in this direction.
            map_[next_pos[0], next_pos[1]] = "X"
            pos = next_pos
        elif next_block == "#":
            dir_ = rotate[(rotate.index(dir_) + 1) % len(rotate)]

    spots_visited = np.sum(map_ == "X") + np.sum(map_ == "^")
    print(map_)
    print(f"{spots_visited=}")


    return spots_visited


if __name__ == "__main__":
    # The test result should be 41.
    # part1("test_input.txt")

    # Too low: 4373
    # Correct: 4374  (forgot to check for the starting spot if we don't cross over it).
    part1("input.txt")
