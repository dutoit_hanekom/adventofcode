# Day 9 Part 1.


def read_input(file_path):
    data_set = list()
    with open(file_path, "r") as file_handle:
        for line in file_handle:
            data_set = [int(chr_) for chr_ in line.strip()]

    return data_set

def expand(file_list):
    expanded: list[int | str] = list()
    empty_spots: list[int] = list()

    id_ = 0
    counter = 0

    for i, num in enumerate(file_list):
        if i % 2 == 0:
            for j in range(num):
                expanded.append(id_)
                counter += 1
            id_ += 1
        else:
            for j in range(num):
                expanded.append(".")
                empty_spots.append(counter)
                counter += 1

    return expanded, empty_spots


def compact(id_list, empty_idxs):

    first_counter = 0
    last_counter = len(id_list) - 1


    while first_counter < last_counter:
        while id_list[first_counter] != ".":
            first_counter += 1
        while id_list[last_counter] == ".":
            last_counter -= 1

        if first_counter >= last_counter:
            break

        # We now have the first empty sport and the last valid number.
        id_list[first_counter] = id_list[last_counter]
        id_list[last_counter] = "."

    return id_list


def part1(data):
    expanded, empty_idxs = expand(data)

    compacted = compact(expanded, empty_idxs)

    checksum = 0
    for i, val in enumerate(compacted):
        if isinstance(val, int):
            checksum += i * val

    print(f"Checksum: {checksum}")

if __name__ == "__main__":
    # The test result should be 1928.
    # data_ = read_input("test_input.txt")

    # Correct: 6340197768906.
    data_ = read_input("input.txt")

    part1(data_)
