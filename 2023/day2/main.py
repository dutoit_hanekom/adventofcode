input_path = r"/day2/input.txt"
# input_path = r"/home/dutoit/PycharmProjects/aoc2023/day2/test_data.txt"

red_cubes = 12
green_cubes = 13
blue_cubes = 14

power = 0

with open(input_path) as input_file:
    for line in input_file:
        tokens = line.split(" ")
        game_number = int(tokens[1][:-1])

        line_ = line.replace(":", ";").strip()
        tokens = line_.split(";")[1:]
        do_sum = True

        max_blue = 0
        max_red = 0
        max_green = 0

        for game in tokens:
            for cubes in game.split(","):
                cubes_ = cubes.strip()
                if "green" in cubes:
                    if int(cubes_.split(" ")[0]) > max_green:
                        max_green = int(cubes_.split(" ")[0])
                if "blue" in cubes:
                    if int(cubes_.split(" ")[0]) > max_blue:
                        max_blue = int(cubes_.split(" ")[0])
                if "red" in cubes:
                    if int(cubes_.split(" ")[0]) > max_red:
                        max_red = int(cubes_.split(" ")[0])

        power += max_red * max_green * max_blue
        print(power)

print(power)
