import numpy as np

input_path = r"2023/day13/input.txt"
# input_path = r"2023/day13/test_input.txt"

maps = list()
current_map = list()
with open(input_path) as input_file:
    for line in input_file:
        if line == "\n":
            maps.append(current_map)
            current_map = list()
        else:
            current_map.append([ch_ for ch_ in line.strip()])

    maps.append(current_map)

maps = [np.array(map_) for map_ in maps]
score = 0
for k, map_ in enumerate(maps):
    rows, cols = map_.shape
    print(f"Map number: {k}")
    found = False

    # Search cols next. Again, don't start at zero. Doesn't make sense.
    # Search for two rows that are the same (or differ by only one char).
    for i in range(1, cols):
        if found:
            break
        fixed_smudge = False
        one = map_[: ,i-1]
        two = map_[:, i]
        # If these two are equal we can search out until we find a diff of one, that should be our smudge.
        if np.array_equal(one, two)or np.sum(np.not_equal(one, two)) == 1:
            if np.sum(np.not_equal(one, two)) == 1:
                fixed_smudge = True
            for j in range(1, min(i, cols - i)):  # search outward from i.
                one_ = map_[:, i-1-j]
                two_ = map_[:, i+j]
                if np.equal(one_, two_).all():
                    continue
                elif np.sum(np.not_equal(one_, two_)) == 1 and not fixed_smudge:
                    # only one diff! Fix that smudge.
                    fixed_smudge = True
                    # print("fixed smudge")
                else:
                    # This may not the one for us.
                    break
            if fixed_smudge:
                print(f"Found mirror and fixed: {fixed_smudge}, col: {i}")
                score += i
                found = True
                break

    if found is True:
        continue

    # Search for two rows that are the same (or differ by only one char).
    for i in range(1, rows):
        if found:
            break
        fixed_smudge = False
        one = map_[i-1, :]
        two = map_[i, :]
        # If these two are equal we can search out until we find a diff of one, that should be our smudge.
        if np.array_equal(one, two) or np.sum(np.not_equal(one, two)) == 1:
            if np.sum(np.not_equal(one, two)) == 1:
                fixed_smudge = True
            for j in range(1, min(i, rows - i)):  # search outward from i.
                one_ = map_[i-1-j, :]
                two_ = map_[i+j, :]
                if np.equal(one_, two_).all():
                    continue
                elif np.sum(np.not_equal(one_, two_)) == 1 and not fixed_smudge:
                    # only one diff! Fix that smudge.
                    fixed_smudge = True
                    # print("fixed smudge")
                else:
                    # This may not the one for us.
                    break
            if fixed_smudge:
                print(f"Found mirror and fixed: {fixed_smudge}, row: {i}")
                score += i * 100
                found = True

print(f"score: {score}")

# 36979 => That's not the right answer; your answer is too high
# 24674 => That's not the right answer; your answer is too low.
# 22012 -> can't be. 24674 was already too low.