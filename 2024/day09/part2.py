# Day 9 Part 2.
from types import SimpleNamespace


class FileBlock(SimpleNamespace):
    empty: bool
    id_: int
    size: int

def read_input(file_path):
    data_set = list()
    with open(file_path, "r") as file_handle:
        for line in file_handle:
            data_set = [int(chr_) for chr_ in line.strip()]

    return data_set

def expand(file_list):
    expanded: list[FileBlock] = list()

    id_ = 0

    for i, num in enumerate(file_list):
        if i % 2 == 0:
            expanded.append(FileBlock(empty=False, id_=id_, size=num))
            id_ += 1
        else:
            expanded.append(FileBlock(empty=True, id_=-1, size=num))

    return expanded


def expand_fully(file_block_list: list[FileBlock]):
    final_ = list()

    for file_block in file_block_list:
        if file_block.id_ >= 0:
            final_ += [file_block.id_] * file_block.size
        else:
            final_ += ["."] * file_block.size

    return final_


def compact(id_list):

    last_counter = len(id_list) - 1

    first_counter = 0
    while last_counter > 0:
        # Find the last file block in the file system.
        while id_list[last_counter].id_ == -1:
            last_counter -= 1

        required_empty_size = id_list[last_counter].size

        # Now - find the first empty block big enough for this file block.
        first_counter = 0
        no_fit = False
        while (not id_list[first_counter].empty) or id_list[first_counter].size < required_empty_size:
            first_counter += 1
            if first_counter >= last_counter:
                # This there isn't a good spot for this:
                no_fit = True
                break

        if no_fit:
            last_counter -= 1
            continue
        if first_counter >= last_counter:
            break

        # Now we should have big enough block (if it exists). If it doesn't ... how do we check that.
        first_empty = id_list[first_counter]
        last_block = id_list[last_counter]
        if first_empty.size == last_block.size:
            # simply replace the whole block - they match perfectly
            first_empty.id_ = last_block.id_
            first_empty.empty = False
            last_block.empty = True
            last_block.id_ = -1
        else:
            # We need to create a new smaller empty block.
            new_empty_block = FileBlock(empty=True, id_=-1, size=first_empty.size - last_block.size)
            # Copy the data from the back to this empty block.
            first_empty.id_ = last_block.id_
            first_empty.empty = False
            first_empty.size = last_block.size
            # Erase the data at the end that's been copied.
            last_block.empty = True
            last_block.id_ = -1
            # Add the new empty block to represent the remaining space.
            id_list.insert(first_counter + 1, new_empty_block)

    return id_list


def part2(data):
    expanded = expand(data)

    compacted = compact(expanded)

    for_crc = expand_fully(compacted)

    checksum = 0
    for i, val in enumerate(for_crc):
        if isinstance(val, int):
            checksum += i * val

    print(f"Checksum: {checksum}")

if __name__ == "__main__":
    # The test result should be 2858.
    # data_ = read_input("test_input.txt")

    # Correct: 6363913128533
    data_ = read_input("input.txt")

    part2(data_)
