"""Set of utils often used for puzzles"""

import numpy as np


def is_in_bounds(grid: np.ndarray, pos: tuple[int, int] | np.ndarray):
    """Check if the given coord is in the bounds of the given grid."""
    if isinstance(pos, np.ndarray):
        assert pos.shape == (2,)
    else:
        # I can actually do this for both ndarray and tuple, right?
        assert len(pos) == 2

    width, height = grid.shape

    return bool(0 <= pos[0] < width and 0 <= pos[1] < height)


def look_around(coord: np.ndarray, bounds: None | tuple[int, ...] = None):
    """Given a coord in a map (grid, 2d array), return all the cardinal coord neighbours.

    Optionally filters out points not within the given optional bounds.
    """
    up = coord - np.array([0, 1])
    down = coord - np.array([0, -1])
    left = coord - np.array([1, 0])
    right = coord - np.array([-1, 0])

    dirs = [up, down, left, right]

    if bounds is not None:
        assert len(bounds) >= 2
        width, height = bounds

        dirs = [dir_ for dir_ in dirs if is_in_bounds(dir_, (width, height))]

    return dirs

