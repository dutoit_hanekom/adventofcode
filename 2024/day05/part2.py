# Day 5, part 2.
from copy import copy


def read_input(file_path):
	order = list()
	updates = list()
	with open(file_path, "r") as file_handle:
		for line in file_handle:
			if "|" in line:
				order.append(tuple([int(val) for val in line.strip().split("|")]))
			elif "," in line:
				updates.append([int(val) for val in line.strip().split(",")])

	return order, updates


def verify_update_rule(update_list: list, rules: list[tuple[int, ...]]) -> tuple[bool, int]:
	"""Given a set of pages to update `update_list`, check this against a set of order `rules`."""

	for i, val in enumerate(update_list):
		next_list = update_list[i+1:]

		for rule in rules:
			if rule[0] == val:
				# Is there a rule for this number? If not - ignore this rule.
				if rule[1] not in update_list:
					continue
				# Make sure the pages coming after this one do not break the rules.
				if rule[1] not in next_list:
					# This breaks the rule. Return idx too, but this turned out to not be super useful.
					return False, i

	return True, 0


def build_valid_list(input_list: list, rules: list[tuple[int, ...]]) -> list:
	"""Given a list of numbers in any order, build an output list of these input values that is valid to the `rules`"""
	# Simply start with the first number.
	working_list = [input_list[0]]

	# Now iterate through the rest, adding them one at a time to a valid position in the list.
	for val in input_list[1:]:
		for i in range(len(working_list) + 1):
			new_list = copy(working_list)
			new_list.insert(i, val)
			# Check if this position is valid...
			result, idx = verify_update_rule(new_list, rules)
			if result:
				# ... if it is, update the working list and move on to the next value.
				working_list = new_list
				break

	return working_list


def part2(file_path):
	order, updates = read_input(file_path)
	total_score = 0

	for update in updates:
		# Is this list in order?
		result, _ = verify_update_rule(update, order)

		if not result:
			# If not in order, fix it and find the valid value.
			fixed_order = build_valid_list(update, order)
			total_score += fixed_order[len(fixed_order) // 2]
			# print(f"{fixed_order=}")

	print(f"{total_score=}")
	return total_score

if __name__ == "__main__":
	# The test result should be 123.
	# part2("test_input.txt")

	# Correct: 6370.
	part2("input.txt")
