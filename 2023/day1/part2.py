# Part 2 correct answer: 55343
import copy

input_path = "2023/day1/input.txt"

words = ["one", "two", "three", "four", "five", "six", "seven", "eight", "nine"]
lookup = {
    "one": "1",
    "two": "2",
    "three": "3",
    "four": "4",
    "five": "5",
    "six": "6",
    "seven": "7",
    "eight": "8",
    "nine": "9",
}

sum_ = 0

# Trying with a replace! Sounds like a good idea but it doesn't work...
# with open(input_path) as input_file:
#     for line in input_file:
#         line_ = copy.copy(line)
#         for word in words:
#             line_ = line_.replace(word, lookup[word])
#
#         numerics = [letter for letter in line_ if letter.isnumeric()]
#
#         combined = numerics[0] + numerics[-1]
#         sum_ += int(combined)

with open(input_path) as input_file:
    for line in input_file:
        first_idx = len(line) + 1

        # Loop through the line looking for the first numeric.
        numerics = [letter for letter in line if letter.isnumeric()]
        if len(numerics) > 0:
            first_num = numerics[0]
            first_idx = line.find(first_num)

        # For each word - go find the first match and see if it's earlier than the previous first.
        for word in words:
            idx = line.find(word)
            if idx >= 0:
                if idx < first_idx:
                    first_num = lookup[word]
                    first_idx = idx

        last_idx = 0

        for letter in reversed(line):
            if letter.isnumeric():
                last_num = letter
                last_idx = line.rfind(last_num)
                break

        for word in words:
            idx = line.rfind(word)
            if idx >= 0:
                if idx > last_idx:
                    last_num = lookup[word]
                    last_idx = idx

        combined = first_num + last_num
        sum_ += int(combined)

print(sum_)
