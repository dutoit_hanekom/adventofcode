# Advent of Code

My [Advent of Code](https://adventofcode.com/) solutions.

There's gonna be some missing things. Like part1's that just become part2 without rename since sometimes I have to rush to try and win.

## Requirements
- Python 3.10+
- Numpy
