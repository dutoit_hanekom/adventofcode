# Day 2 part 2.

def read_input(file_path):
    reports = list()
    with open(file_path, "r") as file_handle:
        for line in file_handle:
            report = line.split(" ")
            report = [int(char.strip()) for char in report]
            reports.append(report)

    return reports

def validate_report(report: list[int]) -> bool:
    creasing = [a - b for a, b in zip(report[1:], report[:-1])]

    # Monotonically increasing or decreasing?
    safe1 = all([val > 0 for val in creasing]) or all([val < 0 for val in creasing])

    safe2 = max([abs(val) for val in creasing]) <= 3

    return safe1 and safe2


def part2(file_path):
    reports = read_input(file_path)

    safe_score = 0
    for report in reports:
        safe = validate_report(report)

        if safe:
            safe_score += 1
        else:
            # If this report isn't safe - brute force thru the list, removing one element at a time to check if it
            # could work with one element removed.
            for i, _ in enumerate(report):
                new_report = report.copy()
                new_report.pop(i)

                if validate_report(new_report):
                    # Removing this one element makes it work, no need to check more.
                    safe_score += 1
                    break


    print(f"safe_score: {safe_score}")

if __name__ == "__main__":
    # The test result should be 4.
    # part2("test_input.txt")

    # 507 is wrong - too high.
    # 446 is wrong - too low.
    # 476 is correct... wow - day 2 part 2 took 3 attempts...
    part2("input.txt")