"""Testing regex examples given stonegpt."""

import re

# An example string containing valid mul, do and don't calls.
text = r"xmul(2,4)&mul[3,7]!^don't()_mul(5,5)+mul(32,64](mul(11,8)undo()?mul(8,5))"

# Let's try a few patterns.
patterns = [
    # Should look for `mul(X, Y)` where X and Y are ints (with optional space between command and Y).
    r"mul\((-?\d+),\s*(-?\d+)\)",
    # Should look for `do()` and `don't()`.
    r"do\(\)|don['’]t\(\)",
    # Should look for `mul(X, Y)` where X and Y are ints AND for `do()` and `don't()`.
    r"mul\((-?\d+),\s*(-?\d+)\)|do\(\)|don['’]t\(\)",
]

for pattern in patterns:
    matches = re.findall(pattern, text)
    print(matches)

r"""
# Results:
## Only mul(X,Y) 
`[('2', '4'), ('5', '5'), ('11', '8'), ('8', '5')]`
The actual result matches the expected result. 
## Only do or don't 
`["don't()", 'do()']`
The actual result matches the expected result. 
## Combo!
Actual result:
`[('2', '4'), ('', ''), ('5', '5'), ('11', '8'), ('', ''), ('8', '5')]`
Notice when we expect "do" or "don't" we get a tuple of empty strings.
Why is this?
"""