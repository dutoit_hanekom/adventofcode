# Day 4 part 1.
import numpy as np


def read_input(file_path):
    map_ = list()
    with open(file_path, "r") as file_handle:
        for line in file_handle:
            chars = list()
            for char in line.strip():
                chars.append(char)
            map_.append(chars)

    return np.array(map_)

def look_for(grid: np.ndarray | list, what: list[str]):
    """Simply look left to right, top to bottom for this string."""
    what_ = "".join(what)
    total = 0
    for row in grid:
        line = "".join(row)
        total += line.count(what_)

    return total


def part1(file_path):
    grid = read_input(file_path)
    total_count = 0
    for _ in range(4):
        total_count += look_for(grid, ["X", "M", "A", "S"])
        grid = np.rot90(grid)

    for _ in range(4):
        diag_grid = list()

        for offset in range(-1*max(grid.shape), max(grid.shape)):
            diag_grid.append(np.diagonal(grid, offset))

        total_count += look_for(diag_grid, ["X", "M", "A", "S"])
        grid = np.rot90(grid)

    print(f"{total_count=}")
    return total_count


if __name__ == "__main__":
    # The test result should be 18.
    # part1("test_input.txt")

    # Correct: 2549
    part1("input.txt")