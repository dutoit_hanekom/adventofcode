from enum import Enum
from typing import Optional, List, Dict
from matplotlib import pyplot as plt
import math

input_path = "2023/day8/input.txt"
# input_path = "2023/day8/test_data.txt"

nodes: Dict[str, tuple] = dict()

with open(input_path) as input_file:
    instructions = input_file.readline().strip()
    # Skip empty line.
    _ = input_file.readline()

    for line in input_file:
        tokens = line.split(" = ")
        key = tokens[0]
        values = tokens[1].split(",")

        nodes[key] = (values[0].replace("(", ""), values[1].strip().replace(")", ""))

current_node = "AAA"

counter = 0
while current_node != "ZZZ":
    fork = nodes[current_node]
    if instructions[counter % len(instructions)] == "L":
        current_node = fork[0]
    else:
        current_node = fork[1]

    counter += 1

print(counter)
