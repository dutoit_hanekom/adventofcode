from enum import Enum
from typing import Optional, List
from matplotlib import pyplot as plt
from timeit import default_timer
import math

input_path = "2023/day6/input.txt"
# input_path = "2023/day6/test_data.txt"
start_time = default_timer()
with open(input_path) as input_file:
    times = input_file.readline()
    times = [token.strip() for token in times.split(" ") if token.strip().isnumeric()]
    time = int("".join(times))
    dists = input_file.readline()
    dists = [token.strip() for token in dists.split(" ") if token.strip().isnumeric()]
    dist = int("".join(dists))

print(f"time {time}; dist {dist}")

i1 = (-time + math.sqrt(time * time + 4 * dist)) / -2
i2 = (-time - math.sqrt(time * time + 4 * dist)) / -2
print(i1, i2)

wins = 0
for i in range(time):
    dist_traveled = (time - i) * i
    if dist_traveled > dist:
        wins += 1

print(wins)
print(f"time taken: {default_timer() - start_time}")
