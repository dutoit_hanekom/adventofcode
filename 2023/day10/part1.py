import numpy as np

input_path = r"/home/dutoit/PycharmProjects/aoc2023/day10/input.txt"
# input_path = r"/home/dutoit/PycharmProjects/aoc2023/day10/test_input.txt"

lines = list()

with open(input_path) as input_file:
    for line in input_file:
        lines.append([chr for chr in line.strip()])


def get_possible_next_pipes(nav_map, block_idx):
    """Looks at the block you're pointing at in the map and finds the two tiles this can go to."""
    block_type = nav_map[block_idx]
    row, col = block_idx
    if block_type == "S":
        # We need to search the four adjacent blocks to see what could connect to this.
        possibilities = list()
        # hmmm.... lets do this ugly first, optimise later.
        try:
            # Look left and check:
            if nav_map_[row, col - 1] == "-" or nav_map_[row, col - 1] == "L" or nav_map_[row, col - 1] == "F":
                possibilities.append((row, col - 1))
        except IndexError:
            pass

        try:
            # Look right and check:
            if nav_map_[row, col + 1] == "-" or nav_map_[row, col + 1] == "J" or nav_map_[row, col + 1] == "7":
                possibilities.append((row, col + 1))
        except IndexError:
            pass

        try:
            # Look up and check:
            if nav_map_[row - 1, col] == "|" or nav_map_[row - 1, col] == "F" or nav_map_[row - 1, col] == "7":
                possibilities.append((row - 1, col))
        except IndexError:
            pass

        try:
            # Look up and check:
            if nav_map_[row + 1, col] == "|" or nav_map_[row + 1, col] == "J" or nav_map_[row + 1, col] == "L":
                possibilities.append((row + 1, col))
        except IndexError:
            pass

        return possibilities
    elif block_type == "|":
        # You can go top and bottom from this one:
        return [(row - 1, col), (row + 1, col)]
    elif block_type == "-":
        # You can go left and right from this one:
        return [(row, col - 1), (row, col + 1)]
    elif block_type == "7":
        return [(row + 1, col), (row, col - 1)]
    elif block_type == "F":
        return [(row + 1, col), (row, col + 1)]
    elif block_type == "J":
        return [(row - 1, col), (row, col - 1)]
    elif block_type == "L":
        return [(row - 1, col), (row, col + 1)]

    return 0


nav_map_ = np.array(lines)

start_idx = np.where(nav_map_ == "S")

# We get the possibilities and just pick one direction to go in. Since this loops, all should be fine.
prev_block = start_idx
current_block = get_possible_next_pipes(nav_map_, start_idx)[0]

# Counter starts at one - we already made one step above.
counter = 1
# Now we loop through the pipe system until we get back to the start.
while nav_map_[current_block] != "S":
    # Get the two ways we can go in.
    ways = get_possible_next_pipes(nav_map_, current_block)

    # One of the ways is the way we came from (prev_idx). We must go in the other direction.
    way_to_go = [way for way in ways if way != prev_block][0]

    # Step to the next block.
    prev_block = current_block
    current_block = way_to_go

    counter += 1

print(counter / 2)
