import numpy as np

testing = False

if testing:
    input_path = r"2023/day11/test_input.txt"
    size_of_space = 10
else:
    input_path = r"2023/day11/input.txt"
    size_of_space = 1000000

lines = list()

with open(input_path) as input_file:
    for line in input_file:
        lines.append([chr for chr in line.strip()])

# Create all of space.
space = np.array(lines)
# This is how big space is.
rows, cols = space.shape

# Space expands on empty rows and cols. So we have to find those.
clear_rows = list()
for row in range(rows):
    if np.all(space[row, :] == "."):
        clear_rows.append(row)
# A set is going to be a little more useful later.
clear_rows = set(clear_rows)

clear_cols = list()
for col in range(cols):
    if np.all(space[:, col] == "."):
        clear_cols.append(col)
# A set is going to be a little more useful later.
clear_cols = set(clear_cols)

# Find every galaxy in space.
galaxy_coods = np.where(space == "#")

total_dist = 0
# We don't have to do the last galaxy. BY the time we get to it the other ones have covered it.
for i, (row, col) in enumerate(zip(galaxy_coods[0][:-1], galaxy_coods[1][:-1])):
    # pair this galaxy with all the other ones counting up. That way we only do every pair once.
    for j in range(i + 1, len(galaxy_coods[0])):
        extra_dist = 0
        row1, col1 = galaxy_coods[0][j], galaxy_coods[1][j]

        # for every border crossing an empty row or column, we have to add a million empty blocks. Rows done now.
        row_bounds = 0
        if np.abs(row1 - row) > 1:  # if there is possible empty space between rows.
            row_range = set(range(min(row, row1) + 1, max(row, row1)))
            empty_boundaries = row_range.intersection(clear_rows)
            row_bounds += len(empty_boundaries)

        # for every border crossing an empty row or column, we have to add a million empty blocks. Cols done now.
        col_bounds = 0
        if np.abs(col1 - col) > 1:  # if there is possible empty space between col.
            # This is the col range between our galaxy pair.
            col_range = set(range(min(col, col1) + 1, max(col, col1)))
            # If our range intersects with the expanded space, we need to add distance later.
            empty_boundaries = col_range.intersection(clear_cols)
            col_bounds += len(empty_boundaries)

        dist = (
            np.abs(row1 - row)
            + np.abs(col1 - col)
            - (row_bounds + col_bounds)
            + size_of_space * (col_bounds + row_bounds)
        )
        total_dist += dist

print(total_dist)

# NOTES:
# For 100, out by: 8492 - 8410 = 82
# For 10, out by: 1112 - 1030 = 82
# Interesting - out by 82 each time?!
# Change the algo
# The error is now for 10: 972 - 1030 = -58
# The error is now for 100: 8352 - 8410 = −58
# 180626
# 746208623963 is too high
# 746208443762 is too high
