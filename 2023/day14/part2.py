import numpy as np

input_path = r"2023/day14/test_input.txt"
# input_path = r"2023/day14/input.txt"

grid_list = list()
with open(input_path) as input_file:
    for line in input_file:
        grid_list.append([ch_ for ch_ in line.strip()])

grid_ = np.array(grid_list)

def tilt_north(grid):
    rows, cols = grid.shape
    for row in range(1, rows):  # skip the top row.
        for col in range(0, cols):
            # If it's a round rock...
            if grid[row, col] == "O":
                # ... then we have to search up to see where the top-most empty spot is.
                i = 1
                for i in range(1, row + 1):
                    if grid[row - i, col] != ".":
                        break  # we found an obstacle.
                else:
                    i += 1
                grid[row, col] = "."
                grid[row - i + 1, col] = "O"
    return grid

def tilt_south(grid):
    rows, cols = grid.shape
    for row in range(rows-2, -1, -1):  # skip the bottom row.
        for col in range(0, cols):
            # If it's a round rock...
            if grid[row, col] == "O":
                # ... then we have to search up to see where the bottom-most empty spot is.
                i = 1
                for i in range(1, rows - row):
                    if grid[row + i, col] != ".":
                        i -= 1
                        break  # we found an obstacle.
                grid[row, col] = "."
                grid[row + i, col] = "O"

    return grid

def tilt_west(grid):
    rows, cols = grid.shape
    for col in range(1, cols):  # skip the top row.
        for row in range(0, rows):
            # If it's a round rock...
            if grid[row, col] == "O":
                # ... then we have to search up to see where the top-most empty spot is.
                i = 1
                for i in range(1, col + 1):
                    if grid[row, col - i] != ".":
                        break  # we found an obstacle.
                else:
                    i += 1
                grid[row, col] = "."
                grid[row, col - i + 1] = "O"

    return grid


def tilt_east(grid):
    rows, cols = grid.shape
    for col in range(cols-2, -1, -1):  # skip the bottom col.
        for row in range(0, rows):
            # If it's a round rock...
            if grid[row, col] == "O":
                # ... then we have to search up to see where the east-most empty spot is.
                i = 1
                for i in range(1, cols - col):
                    if grid[row, col + i] != ".":
                        i -= 1
                        break  # we found an obstacle.
                grid[row, col] = "."
                grid[row, col + i] = "O"

    return grid


def cycle(grid):
    grid = tilt_north(grid)
    grid = tilt_west(grid)
    grid = tilt_south(grid)
    grid = tilt_east(grid)

    return grid

# load = 0
# for i, row in enumerate(grid):
#     load += (rows - i) * np.sum(np.equal(row, "O"))

# This is to stop numpy from shortening my string representation and losing data in the process.
np.set_printoptions(threshold=np.inf)

repeating = False
counter = 0
mash = dict()
massh = set()
while not repeating:
    grid_str = np.array_str(grid_)
    if grid_str in massh:
        break
    mash[counter] = grid_.copy()
    massh.add(grid_str)
    grid_ = cycle(grid_)
    counter += 1

start_key = 0
for key in mash.keys():
    if np.array_equal(grid_, mash[key]):
        start_key = key

repeats = 64
the_key = (repeats - start_key) % (counter -1)

grid_ = mash[the_key]

load = 0
rows, cols = grid_.shape
for i, row in enumerate(grid_):
    load += (rows - i) * np.sum(np.equal(row, "O"))
print(load)

# That's not the right answer; your answer is too high. 99698
# That's not the right answer; your answer is too high. 99539
# That's not the right answer; your answer is too high. 99507
# Also wrong: 99266
#