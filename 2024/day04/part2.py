# Day 4 part 2.
import numpy as np


def read_input(file_path):
    map_ = list()
    with open(file_path, "r") as file_handle:
        for line in file_handle:
            chars = list()
            for char in line.strip():
                chars.append(char)
            map_.append(chars)

    return np.array(map_)

def match(array: np.ndarray, pattern: np.ndarray) -> bool:
    assert array.shape == pattern.shape

    rows, cols = array.shape

    does_match = True

    for row in range(rows):
        for col in range(cols):
            if pattern[row, col] != "." and array[row, col] != pattern[row, col]:
                does_match = False

    return does_match


def look_for(grid: np.ndarray, what: np.ndarray) -> int:
    """Simply look left to right, top to bottom for this string."""
    total = 0
    width, height = what.shape

    for row in range(grid.shape[0] - width + 1):
        for col in range(grid.shape[1] - height + 1):
            subset = grid[row: row + width, col: col + height]

            if match(subset, what):
                total += 1

    return total


def part2(file_path):
    grid = read_input(file_path)
    pattern = np.array([['M', '.', 'S'],
                        ['.', 'A', '.'],
                        ['M', '.', 'S']])

    total = 0
    for _ in range(4):
        total += look_for(grid, pattern)
        pattern = np.rot90(pattern)

    print(f"{total=}")
    return total

if __name__ == "__main__":
    # The test result should be 9.
    # part2("test_input.txt")

    # Correct: 2003
    part2("input.txt")
