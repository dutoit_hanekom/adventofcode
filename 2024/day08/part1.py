# Day 8 Part 1.
from itertools import combinations
import numpy as np

def read_input(file_path):
    data_set = list()
    with open(file_path, "r") as file_handle:
        for line in file_handle:
            vals = [chr_ for chr_ in line.strip()]
            data_set.append(vals)

    return np.array(data_set)

def is_in_bounds(grid: np.ndarray, pos: tuple[int, int] | np.ndarray):
    if isinstance(pos, np.ndarray):
        assert pos.shape == (2,)
    else:
        # I can actually do this for both ndarray and pos, right?
        assert len(pos) == 2

    width, height = grid.shape

    return bool(0 <= pos[0] < width and 0 <= pos[1] < height)


def part1(file_path):
    map_ = read_input(file_path)

    antenna_types: list[str] = list(np.unique(map_))
    # We won't want the "." - it's background.
    antenna_types.remove(".")

    unique_locations = set()

    for antenna_type in antenna_types:
        locations = list(np.argwhere(map_ == antenna_type))

        for ann1, ann2 in combinations(locations, 2):
            delta = ann2 - ann1

            antinode1 = ann1 - delta
            antinode2 = ann2 + delta

            if is_in_bounds(map_, antinode1):
                unique_locations.add((antinode1[0], antinode1[1]))
            if is_in_bounds(map_, antinode2):
                unique_locations.add((antinode2[0], antinode2[1]))

            # print("Breakpoint")

    print(f"Number of unique locations: {len(unique_locations)}")

    # print(map_)


if __name__ == "__main__":
    # The test result should be 14.
    # part1("test_input.txt")

    # Correct: 371
    part1("input.txt")