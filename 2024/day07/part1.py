# Day 7 Part 1.
def read_input(file_path):
    data_set = list()
    with open(file_path, "r") as file_handle:
        for line in file_handle:
            answer, vals_ = line.strip().split(":")
            vals = [int(val) for val in vals_.split(" ") if val != ""]
            data_set.append((int(answer), vals))

    return data_set

def eval_(nums: list[int], ops: list[str]) -> int:
    assert len(nums) - 1 == len(ops)
    ans = nums[0]

    for num, op in zip(nums[1:], ops):
        if op == "*":
            ans *= num
        elif op == "+":
            ans += num

    return  ans

def incr_ops(length, number):
    # +2 for `0b...`
    binary = format(number, f"#0{length+2}b")
    ops = ["*" if chr_ == "1" else "+" for chr_ in binary[2:]]

    return ops



def part1(path):
    calib_data = read_input(path)

    score = 0

    for ans, nums in calib_data:
        # We'll need this many ops.
        num_ops = len(nums) - 1

        for i in range(2**num_ops):
            ops = incr_ops(num_ops, i)
            if eval_(nums, ops) == ans:
                score += ans
                # This one works - so skip the rest
                break

        print(f"Solved")
    print(f"{score=}")


if __name__ == "__main__":
    # The test result should be 3749.
    # part1("test_input.txt")

    # Correct: 12839601725877
    part1("input.txt")