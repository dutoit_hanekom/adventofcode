import numpy as np

# input_path = r"2023/day13/input.txt"
input_path = r"2023/day13/test_input.txt"

maps = list()
current_map = list()
with open(input_path) as input_file:
    for line in input_file:
        if line == "\n":
            maps.append(current_map)
            current_map = list()
        else:
            current_map.append([ch_ for ch_ in line.strip()])

    maps.append(current_map)

maps = [np.array(map_) for map_ in maps]
score = 0
for k, map_ in enumerate(maps):
    rows, cols = map_.shape
    print(f"Map number: {k}")

    # TODO: Optimize: We can probably just look for rows for cols that are equal next to each other. No need
    # to search for large sections?

    # search rows first?
    # The is no point to start a zero. We have to start at a point with something
    # to compare.
    for i in range(1, rows):
        gap = min(i, rows - i)
        left = map_[i - gap:i, :]
        right = map_[i: i+gap, :]

        if np.array_equal(left, np.flip(right, axis=0)):
            print(f"Found mirror rows {i}")
            score += i * 100

    # Search cols next. Again, don't start at zero. Doesn't make sense.
    for i in range(1, cols):
        gap = min(i, cols - i)
        top = map_[:, i - gap:i]
        bottom = map_[:, i: i+gap]

        if np.array_equal(top, np.flip(bottom, axis=1)):
            print(f"Found mirror cols {i}")
            score += i

print(f"score: {score}")
