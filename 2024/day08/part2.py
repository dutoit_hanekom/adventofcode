# Day 8 Part 2.
from timeit import default_timer
from itertools import combinations
import numpy as np

def read_input(file_path):
    data_set = list()
    with open(file_path, "r") as file_handle:
        for line in file_handle:
            vals = [chr_ for chr_ in line.strip()]
            data_set.append(vals)

    return np.array(data_set)

def is_in_bounds(grid: np.ndarray, pos: tuple[int, int] | np.ndarray):
    if isinstance(pos, np.ndarray):
        assert pos.shape == (2,)
    else:
        # I can actually do this for both ndarray and pos, right?
        assert len(pos) == 2

    width, height = grid.shape

    return bool(0 <= pos[0] < width and 0 <= pos[1] < height)


def part2(map_):
    antenna_types: list[str] = list(np.unique(map_))
    # We won't want the "." - it's background.
    antenna_types.remove(".")

    # Set to keep track of unique locations that contain antinodes.
    unique_locations = set()

    for antenna_type in antenna_types:
        locations = list(np.argwhere(map_ == antenna_type))

        if len(locations) == 1:
            print("Only one antenna - no antinodes.")
            continue

        for ann1, ann2 in combinations(locations, 2):
            delta = ann2 - ann1

            unique_locations.add((ann1[0], ann1[1]))
            unique_locations.add((ann2[0], ann2[1]))

            antinode1 = ann1 - delta
            while is_in_bounds(map_, antinode1):
                unique_locations.add((antinode1[0], antinode1[1]))
                antinode1 = antinode1 - delta

            antinode2 = ann2 + delta
            while is_in_bounds(map_, antinode2):
                unique_locations.add((antinode2[0], antinode2[1]))
                antinode2 = antinode2 + delta

    print(f"Number of unique locations: {len(unique_locations)}")


if __name__ == "__main__":
    # The test result should be 34.
    # grid_ = read_input("test_input.txt")

    # Too low: 1097
    # Correct: 1229. I didn't realise that the antenna counts as an antinode too.
    grid_ = read_input("input.txt")

    start_time = default_timer()
    part2(grid_)
    print(f"Time taken: {default_timer() - start_time}")