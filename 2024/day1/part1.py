# Day 1 Part 1.

def read_input(file_path):
    map1, map2 = list(), list()
    with open(file_path, "r") as file_handle:
        for line in file_handle:
            str1, str2 = line.split("   ")
            map1.append(int(str1.strip()))
            map2.append(int(str2.strip()))

    return map1, map2


def part1(file_path):
    map1, map2 = read_input(file_path)

    # Sort the maps.
    map1 = sorted(map1)
    map2 = sorted(map2)

    total_dist = 0
    for val1, val2 in zip(map1, map2):
        dist = abs(val2 - val1)

        total_dist += dist

    print(f"Dist: {total_dist}")


if __name__ == "__main__":
    part1("input.txt")