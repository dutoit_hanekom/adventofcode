# Day 10 Part 1.
import numpy as np


def read_input(file_path):
    data_set = list()
    with open(file_path, "r") as file_handle:
        for line in file_handle:
            data_set.append([int(chr_) for chr_ in line.strip()])

    return np.array(data_set)


def look_around(coord: np.ndarray, map_: np.ndarray):
    up = coord - np.array([0, 1])
    down = coord - np.array([0, -1])
    left = coord - np.array([1, 0])
    right = coord - np.array([-1, 0])

    width, height = map_.shape

    current_height = map_[coord[0], coord[1]]

    options = list()

    for dir_ in [up, down, left, right]:
        # If in bounds:
        if 0 <= dir_[0] < width and 0 <= dir_[1] < height:
            step_height = map_[dir_[0], dir_[1]] - current_height
            if step_height == 1:
                options.append(dir_)

    return options


def find_trails(trailhead_coord: np.ndarray, map_: np.ndarray) -> int:
    current_coord = trailhead_coord
    ends = set()

    possibilities = look_around(current_coord, map_)

    while len(possibilities) != 0:
        current_coord = possibilities.pop(0)

        if map_[current_coord[0], current_coord[1]] == 9:
            ends.add((int(current_coord[0]), int(current_coord[1])))
        else:
            possibilities += look_around(current_coord, map_)

    return len(ends)


def part1(map_):
    score = 0

    trailheads = np.argwhere(map_ == 0)

    for trailhead in trailheads:
        trails = find_trails(trailhead, map_)

        score += trails

    print(f"score: {score}")

if __name__ == "__main__":
    # The test result should be 36.
    # data_ = read_input("test_input.txt")

    # Correct: 461.
    data_ = read_input("input.txt")

    part1(data_)
