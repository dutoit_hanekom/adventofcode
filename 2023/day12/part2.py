from typing import List


class Record:
    def __init__(self, springs: str = None, crcs: List[int] = None):
        self.springs = springs
        self.crcs = crcs

    def match(self, springs) -> bool:
        """
        Verifies if the springs match the CRC.
        :return: True for a match, False for a missmatch.
        """
        groups = springs.split(".")
        groups = [group for group in groups if group != ""]
        # TODO: Probably check each group for "?"?
        # If the length of the groups aren't the same, they can't match... right? Yes. Maybe.
        if len(groups) != len(self.crcs):
            return False

        for group, crc in zip(groups, self.crcs):
            if len(group) != crc:
                return False

        return True

    def find_matches(self):
        """
        Find a match[es?] for this spring string and it's CRC.
        :return:
        """
        bits = self.springs.count("?")
        match_counter = 0

        for i in range(2**bits):
            code = f"{i:0{bits}b}"
            code = code.replace("0b", "")
            code = code.replace("0", ".")
            code = code.replace("1", "#")

            new_springs = self.springs
            for ch_ in code:
                new_springs = new_springs.replace("?", ch_, 1)

            if self.match(new_springs):
                match_counter += 1

        return match_counter


    def add_crc(self, crc):
        if self.crcs is None:
            self.crcs = list()

        self.crcs.append(crc)

    def __str__(self):
        return self.springs + " " + ",".join([str(num) for num in self.crcs])


# input_path = r"2023/day12/input.txt"
input_path = r"2023/day12/test_input.txt"

records: List[Record] = list()

with open(input_path) as input_file:
    for line in input_file:
        tokens = line.split(" ")
        spring_list = tokens[0]
        expanded_spring_list = [spring_list] * 5
        expanded_spring_list = "?".join(expanded_spring_list)
        crc_list = tokens[1]
        crc_list = [int(token.strip()) for token in crc_list.split(",")] * 5
        records.append(Record(springs=expanded_spring_list, crcs=crc_list))

for record in records:
    print(record.find_matches())

# print(sum([record.find_matches() for record in records]))
print("breakpoint")
