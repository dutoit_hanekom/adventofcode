input_path = "2023/day4/input.txt"
# input_path = "2023/day4/test_input.txt"

card_win_lookup = dict()

with open(input_path) as input_file:
    for i, line in enumerate(input_file):
        bar = line.find("|")
        sep = line.find(":")

        card_num = i + 1

        winning_nums = line[sep:bar].split(" ")
        winning_nums = set([int(num) for num in winning_nums if num.isnumeric()])

        my_nums = line[bar:].split(" ")
        my_nums = set([int(num.strip()) for num in my_nums if num.strip().isnumeric()])

        overlap = winning_nums.intersection(my_nums)
        overlap = len(overlap)

        card_win_lookup[card_num] = overlap

# buff = list()
buff = list(card_win_lookup.keys())
increasing = True
current_idx = 0

while current_idx < len(buff):
    current_size = len(buff)

    card_to_check = buff[current_idx]
    score = card_win_lookup[card_to_check]
    for i in range(card_to_check + 1, card_to_check + score + 1):
        buff.append(i)

    current_idx += 1

    increasing = len(buff) > current_size

buff = sorted(buff)
print(len(buff))
