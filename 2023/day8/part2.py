from enum import Enum
from typing import Optional, List, Dict
from matplotlib import pyplot as plt
import math
import numpy as np
from timeit import default_timer

input_path = "2023/day8/input.txt"
# input_path = "2023/day8/test_data.txt"

nodes: Dict[str, tuple] = dict()

start_time = default_timer()

with open(input_path) as input_file:
    instructions = input_file.readline().strip()
    # Skip empty line.
    _ = input_file.readline()

    for line in input_file:
        tokens = line.split(" = ")
        key = tokens[0]
        values = tokens[1].split(",")

        nodes[key] = (values[0].replace("(", ""), values[1].strip().replace(")", ""))

print(f"Parsing file took: {default_timer() - start_time}s")


def get_steps_count(start_node_, nodes_, instructions_):
    current_node = start_node_

    counter = 0
    while not current_node.endswith("Z"):
        fork = nodes_[current_node]
        if instructions_[counter % len(instructions_)] == "L":
            current_node = fork[0]
        else:
            current_node = fork[1]

        counter += 1

    return counter


compute_start_time = default_timer()
# All the start nodes (ones that end in A)
start_nodes = [node for node in nodes.keys() if node.endswith("A")]
# Find all the shorest paths.
shortest_paths = [get_steps_count(node, nodes, instructions) for node in start_nodes]
print(f"Compute time: {default_timer() - compute_start_time}")

lcm_start_time = default_timer()
print(np.lcm.reduce(shortest_paths))
print(f"LCM time: {default_timer() - lcm_start_time}")

print(shortest_paths)

# I think my correct answer is: 19185263738117
