# Day 1 Part 2.

def read_input(file_path):
    map1, map2 = list(), list()
    with open(file_path, "r") as file_handle:
        for line in file_handle:
            str1, str2 = line.split("   ")
            map1.append(int(str1.strip()))
            map2.append(int(str2.strip()))

    return map1, map2


def part2(file_path):
    map1, map2 = read_input(file_path)

    sim_score = 0
    for val in map1:
        count = map2.count(val)

        sim_score += val * count

    print(f"Dist: {sim_score}")


if __name__ == "__main__":
    # The test result should be 31.
    # part2("test_input.txt")

    part2("input.txt")