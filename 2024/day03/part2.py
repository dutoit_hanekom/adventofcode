# Day 3 part 2.
import re

# pattern = r"mul\((-?\d+),\s*(-?\d+)\)"
pattern = r"mul\((-?\d+),\s*(-?\d+)\)|do\(\)|don['’]t\(\)"
pattern2 = r"do\(\)|don['’]t\(\)"

def read_input(file_path):
    instructions = list()
    with open(file_path, "r") as file_handle:
        for line in file_handle:
            matches = re.findall(pattern, line)
            matches2 = re.findall(pattern2, line)

            counter = 0
            for instr in matches:
                if instr[0] != "" and instr[1] != "":
                    instructions.append((int(instr[0]), int(instr[1])))
                else:
                    instructions.append(matches2[counter])
                    counter += 1

    return instructions

def part1(file_path):
    instructions = read_input(file_path)

    sum_total = 0
    state = True
    for instr in instructions:
        if state and isinstance(instr, tuple):
            sum_total += instr[0] * instr[1]
        elif isinstance(instr, str):
            if instr == "do()":
                state = True
            elif instr == "don't()":
                state = False
            else:
                print("This shouldn't happen")

    print(f"{sum_total=}")

if __name__ == "__main__":
    # The test result should be 48.
    # part1("test_input_2.txt")

    # Correct: 88811886
    part1("input.txt")