# Day 3 part 1.
import re


pattern = r"mul\((-?\d+),\s*(-?\d+)\)"

def read_input(file_path):
    instructions = list()
    with open(file_path, "r") as file_handle:
        for line in file_handle:
            data_set = re.findall(pattern, line)

            for instr in data_set:
                instructions.append((int(instr[0]), int(instr[1])))


    return instructions

def part1(file_path):
    instructions = read_input(file_path)

    sum_total = 0
    for instr in instructions:
        sum_total += instr[0] * instr[1]

    print(f"{sum_total=}")

if __name__ == "__main__":
    # The test result should be 161.
    # part1("test_input.txt")

    # sum_total = 166357705
    part1("input.txt")
